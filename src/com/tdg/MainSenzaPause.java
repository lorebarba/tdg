


package com.tdg;

public class MainSenzaPause {

    private static double durataBattuta;
    private static double n;
    private static double nDiffRytm;
    private static double[] valori;
    private static double minRitmo;
    private static double tempN;
    private static double totale;

    public static void main(String[] args) {
        // 4/4
        durataBattuta = 1;
        n = 4; // # caselle nella battuta
        valori = new double[]{1, 2, 4};
        totale = 0;
        // 4 bit
        // 0000   ->  1111


        // for ( da 0000 a 1111)


        //sommo tutte le combinazioni costituite dallo stesso ritmo
        for (int i = 0; i < valori.length; i++) {
            totale += 2 * Math.pow(2, valori[i]);
        }


        for (int comboBiggestValue = 2; comboBiggestValue < 3; comboBiggestValue++) {

        }

        //combo (1/2, 1/4)                      OOOK
        for (int i = 0; i < n - 1; i++) {
            totale += 2 * Math.pow(2, 2);
        }

        //combo (1/4, 1/8)
        for (int i = 0; i < n - 1; i++) {
            totale += 2 * Math.pow(2, n - 2);
        }

        //combo (1/2, 1/8)
        for (int i = 0; i < n - 1; i++) {
            totale += 2 * Math.pow(2, 2);
        }

        //combo (1/2, 1/4, 1/8)
        for (int i = 0; i < n - 1; i++) {
            totale += 2 * Math.pow(2, 2);
        }

    }

    // 1/4      1/8
    public static double combo(double a, double b) {
        double temp = 0;
        for (int i = 0; i < n - 1; i++) {
            temp += 2 * Math.pow(2, n - (a / b));
        }
        return temp;
    }

    //                          1/4       1/8
    public static double combo2(double maxVal, double granularita) {
        double temp = 0;
        for (int i = 0; i < n - Math.pow(maxVal, -1) + 1; i++) {
            temp += 2 * Math.pow(2, n - (maxVal / Math.pow(granularita, -1)));
        }
        return temp;
    }
}
